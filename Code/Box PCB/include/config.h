/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef PB_CONFIG_H
#define PB_CONFIG_H

bool realtime; // test 

#define GPSSerial Serial1

// Set GPSECHO to 'false' to turn off echoing the GPS data to the Serial console
// Set to 'true' if you want to debug and listen to the raw GPS sentences
#define GPSECHO false 

//to store GGA & RMC 
String NMEA1; 
String NMEA2;

// control pins for relays
#define CTRL_FCpin 44           //Fuel Cell Relay
#define CTRL_EVC500pin 43       //EVC500 Relay
#define CTRL_PreChargepin 42    //PreCharge Relay
#define CTRL_Charger 41         // Charger Relay

//interrupt pins 
#define PreChargeEndSignal_pin 4  // 5 V 
#define Sense24V_pin 5      // 24 V 
#define SenseKillswitch_pin 6    // 24 V
#define Opt1_pin 18
#define Opt2_pin 19

bool SenseKillswitch_state;
bool Sense24V_state;
bool PreChargeEndSignal_state;
bool Opt1_state;
bool Opt2_state;

bool  PreCharge_relay_message;
bool  Motor_relay_message;
bool  FC_relay_message;
bool  Sense24_message;
bool  Kill_message;

//uses |    (bitwise operators)
#define set1_PreCharge_Relay 1
#define set1_Motor_Relay  2
#define set1_FuelCell_Relay  4
#define set1_24_Sense  8  //aka fuelcell sense 
#define set1_killswitch 16  

//uses &    (bitwise operators)
#define set0_PreCharge_Relay  14
#define set0_Motor_Relay  13
#define set0_FuelCell_Relay  11
#define set0_24_Sense  7
#define set0_killswitch 15 

//to read sentences from GPS
char read; 

// aux for conversion
nmea_float_t Latitude_DM;
nmea_float_t Longitude_DM;

// global vars for CAN messages
nmea_float_t Latitude_DD;
nmea_float_t Longitude_DD;

nmea_float_t speed;
nmea_float_t angle;
int8_t status;  

#endif