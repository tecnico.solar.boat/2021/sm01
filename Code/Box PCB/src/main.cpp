/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>
#include <Timer.h> 
#include "Adafruit_GPS.h"
#include "../include/config.h"
#include "CAN_TSB_H2.h"

Adafruit_GPS GPS(&GPSSerial);

CAN_message_t PB_GPS;
CAN_message_t PB_Vel_Angle_Status;

TSB_CAN_H2 MCan;

Timer t;
Timer second_after;
Timer second_after_2;

int ledPin = 13;
int pause = 1000;

void init_CAN_m();
void send_CAN_m(void* context); 
void Sense_Killswitch_ISR();
void PreCharge_EndSignal_ISR();
void Sense24V_ISR();
void readGPS();
void clearGPS();

void FC_OFF_Charger_ON(void* context);
void FC_ON_Charger_OFF(void* context);

void setup() {
  // put your setup code here, to run once:
  //realtime = LOW;
  Serial.begin(115200);
  GPS.begin(9600);

	status = 0;
  // RMC (recommended minimum) and GGA (fix data) -> sentences
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); // 1 Hz update rate
  // For the parsing code to work nicely and have time to sort thru the data, and
  // print it out we don't suggest using anything higher than 1 Hz

    pinMode(Sense24V_pin, INPUT); 
	pinMode(SenseKillswitch_pin,INPUT);
	pinMode(PreChargeEndSignal_pin, INPUT);
	pinMode(Opt1_pin,INPUT);
	pinMode(Opt2_pin,INPUT);

	pinMode(CTRL_PreChargepin,OUTPUT);
	pinMode(CTRL_EVC500pin,OUTPUT);
	pinMode(CTRL_FCpin,OUTPUT);
	pinMode(CTRL_Charger,OUTPUT);

	pinMode(ledPin,OUTPUT);

	if( Can0.init(CAN_BPS_500K)){
			Serial.println("CAN Init OK!");
		}
		else{
			Serial.println("CAN Init Failed!");
		}
	Can0.attachObj(&MCan);

	Can0.setNumTXBoxes(1);    // Use all MOb (only 6x on the ATmegaxxM1 series) for receiving.

		//standard  
		Can0.setRXFilter(1, 0, 0, false);       //catch all mailbox
		Can0.setRXFilter(2, 0, 0, false);       //catch all mailbox
		Can0.setRXFilter(3, 0, 0, false);       //catch all mailbox
		Can0.setRXFilter(4, 0, 0, false);       //catch all mailbox
		Can0.setRXFilter(5, 0, 0, false);       //catch all mailbox

	init_CAN_m();
	Serial.print("\n______RESET_______\n");

	attachInterrupt(digitalPinToInterrupt(SenseKillswitch_pin), Sense_Killswitch_ISR,CHANGE);
	attachInterrupt(digitalPinToInterrupt(PreChargeEndSignal_pin), PreCharge_EndSignal_ISR,RISING);
	attachInterrupt(digitalPinToInterrupt(Sense24V_pin),Sense24V_ISR,CHANGE); 
	
	digitalWrite (CTRL_Charger,HIGH);

	t.every(500,send_CAN_m,(void*)1);
	t.oscillate(ledPin, pause, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
	clearGPS();
	read = GPS.read(); // not sure if its okay to interrupt GPS readings

	if(GPS.newNMEAreceived())
		readGPS();	

	second_after.update();
	second_after_2.update();
	t.update();
}

void readGPS(){ 
  
	if (!GPS.parse(GPS.lastNMEA())) {
      return; //leaves function if parsing fails
	}
	Serial.println("parsed");
	NMEA1=GPS.lastNMEA();
	Serial.println(NMEA1);
	Serial.println("");
	Serial.println(GPS.latitude,6);
	Serial.println(GPS.longitude,6); 
	if (GPS.fix){
		speed = GPS.speed; 
		Latitude_DM = GPS.latitude;
		Longitude_DM = GPS.longitude;
		angle = GPS.angle; 
													// latitude 3844.231445 -> 38.7397
		Latitude_DD = (int) (Latitude_DM/100.0); //latitude dd = 38 
		Latitude_DD += ((((Latitude_DM/100.0) - (Latitude_DD))*100)/60.0); // 38 + ()
		if (GPS.lat=='S')
			Latitude_DD *= -1;

		Longitude_DD = (int) (Longitude_DM/100.0);
		Longitude_DD += ((((Longitude_DM/100.0) - (Longitude_DD))*100)/60.0);
		if (GPS.lon=='W')
			Longitude_DD *= -1;
	}

	Serial.println(Latitude_DD,6);
	Serial.println(Longitude_DD,6);

}

void clearGPS(){ //clears buffer
	if (GPS.newNMEAreceived()) {
		read=GPS.read();
	}
	GPS.parse(GPS.lastNMEA());

	if (GPS.newNMEAreceived()) {
		read=GPS.read();
	}
	GPS.parse(GPS.lastNMEA());

}

void init_CAN_m(){

  //latitude & longitude
    PB_GPS.id=0x618;
	PB_GPS.len = 8;

  //speed angle status 
    PB_Vel_Angle_Status.id=0x628;
	PB_Vel_Angle_Status.len = 5;

}


void FC_OFF_Charger_ON(void* context){  //
	digitalWrite(CTRL_FCpin,LOW);
}

void FC_ON_Charger_OFF(void* context){
	digitalWrite(CTRL_Charger,LOW);
}

void Sense_Killswitch_ISR(){
	//change
	SenseKillswitch_state = digitalRead(SenseKillswitch_pin); 
	if (SenseKillswitch_state ==LOW){
		//opens motor relay					
		digitalWrite(CTRL_EVC500pin,LOW);

		Motor_relay_message = false;

		Kill_message = false;
	}else{
		digitalWrite(CTRL_PreChargepin,HIGH);

		PreCharge_relay_message = true;

		Kill_message = true;
	} 
}

void PreCharge_EndSignal_ISR(){
	//rising
	if (SenseKillswitch_state == 1){
	digitalWrite(CTRL_EVC500pin,HIGH);  //K2
	digitalWrite(CTRL_PreChargepin,LOW); //K3
	PreChargeEndSignal_state = HIGH;
	}
}

void Sense24V_ISR(){
	Sense24V_state = digitalRead(Sense24V_pin); 
	if (Sense24V_state == HIGH){
		digitalWrite(CTRL_FCpin,HIGH);  //K1
		second_after.after(1000,FC_ON_Charger_OFF,(void*)1);

		FC_relay_message = true;
		Sense24_message = true;

	}else{
		digitalWrite(CTRL_Charger,HIGH);
		second_after_2.after(1000,FC_OFF_Charger_ON,(void*)1);
		FC_relay_message = false;
		Sense24_message = false;
	}
}

void send_CAN_m(void* context){

	status = (Kill_message<<4) + (Sense24_message<<3) + (FC_relay_message<<2) + (Motor_relay_message << 1) + (PreCharge_relay_message);

	int32_t ind = 0;

	buffer_append_float32(PB_GPS.buf,Latitude_DD,pow(2,24),&ind);
	buffer_append_float32(PB_GPS.buf,Longitude_DD,pow(2,23),&ind);
	Can0.write(PB_GPS);

	ind = 0;

	buffer_append_int16(PB_Vel_Angle_Status.buf,speed,&ind);
	buffer_append_uint16(PB_Vel_Angle_Status.buf,angle,&ind);
	buffer_append_uint8(PB_Vel_Angle_Status.buf,status,&ind);
	Can0.write(PB_Vel_Angle_Status);

	Serial.print("\n Sense 24V message: ");	
	Serial.print(Sense24_message);
	Serial.print("\n Kills message: ");	
	Serial.print(Kill_message);
	Serial.print("\n Motor state atual: ");	
	Serial.print(Motor_relay_message);
	Serial.print("\n PreCharge state atual: ");
	Serial.print(PreCharge_relay_message);
	Serial.print("\n FC state atual: ");
	Serial.print(FC_relay_message);
	
	Serial.print("\n status: ");
	Serial.print(status);
	Serial.print("\n");

}

