// Update fault_string based on fault
var fault = getVariableValue("Fault");

switch(fault) {
    case 0:
        setVariableValue("Fault_string","NONE");
        break;
    case 1:
        setVariableValue("Fault_string","OVER_VOLTAGE");
        break;        
    case 2:
        setVariableValue("Fault_string","UNDER_VOLTAGE");
        break;        
    case 3:
        setVariableValue("Fault_string","DRV");
        break;        
    case 4:
        setVariableValue("Fault_string","ABS_OVER_CURRENT");
        break;            
    case 5:
        setVariableValue("Fault_string","OVER_TEMP_FET");
        break;            
    case 6:
        setVariableValue("Fault_string","OVER_TEMP_MOTOR");
        break;            
    case 7:
        setVariableValue("Fault_string","GATE_DRIVER_OVER_VOLTAGE");
        break;     
    case 8:
        setVariableValue("Fault_string","GATE_DRIVER_UNDER_VOLTAGE");
        break;            
    case 9:
        setVariableValue("Fault_string","MCU_UNDER_VOLTAGE");
        break;            
    case 10:
        setVariableValue("Fault_string","BOOTING_FROM_WATCHDOG_RESET");
        break;            
    case 11:
        setVariableValue("Fault_string","ENCODER_SPI");
        break;     
    case 12:
        setVariableValue("Fault_string","ENCODER_SINCOS_BELOW_MIN_AMPLITUDE");
        break;            
    case 13:
        setVariableValue("Fault_string","ENCODER_SINCOS_ABOVE_MAX_AMPLITUDE");
        break;            
    case 14:
        setVariableValue("Fault_string","FLASH_CORRUPTION");
        break;            
    case 15:
        setVariableValue("Fault_string","HIGH_OFFSET_CURRENT_SENSOR_1");
        break;     
    case 16:
        setVariableValue("Fault_string","HIGH_OFFSET_CURRENT_SENSOR_2");
        break;            
    case 17:
        setVariableValue("Fault_string","HIGH_OFFSET_CURRENT_SENSOR_3");
        break;            
    case 18:
        setVariableValue("Fault_string","UNBALANCED_CURRENTS");
        break;            
    case 19:
        setVariableValue("Fault_string","BRK");
        break;   
    case 20:
        setVariableValue("Fault_string","RESOLVER_LOT");
        break;     
    case 21:
        setVariableValue("Fault_string","RESOLVER_DOS");
        break;            
    case 22:
        setVariableValue("Fault_string","RESOLVER_LOS");
        break;            
    case 23:
        setVariableValue("Fault_string","FLASH_CORRUPTION_APP_CFG");
        break;            
    case 24:
        setVariableValue("Fault_string","FLASH_CORRUPTION_MC_CFG");
        break;  
    case 25:
        setVariableValue("Fault_string","ENCODER_NO_MAGNET");
        break;     
    default:
        setVariableValue("Fault_string","NOT RECOGNIZED ERROR");
        break;         
}