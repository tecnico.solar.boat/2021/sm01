// Update RTC clock with UTC clock

// Get Timezone
timezone = getVariableValue("TimeZone");

switch (timezone)
{
    case 0:
        timezone = -6;
        break;
    case 1:
        timezone = -5;
        break;
    case 2:
        timezone = -4;
        break;
    case 3:
        timezone = -3;
        break;
    case 4:
        timezone = -2;
        break;
    case 5:
        timezone = -1;
        break;
    case 6:
        timezone = 0;
        break;
    case 7:
        timezone = 1;
        break;
    case 8:
        timezone = 2;
        break;
    case 9:
        timezone = 3;
        break;
    case 10:
        timezone = 4;
        break;
    case 11:
        timezone = 5;
        break;
    case 12:
        timezone = 6;
        break;
    default:
        timezone = 0;
        break;     
}

// Internet clock UTC
secs = getVariableValue("Time"); // seconds from 1/1/1970
secs = secs + timezone * 3600;
milisecs = secs * 1000; // miliseconds from 1/1/1970


date = new Date(milisecs);

year = date.getFullYear();
month = date.getMonth();
day = date.getDay();
hour = date.getHours();
min = date.getMinutes();
sec = date.getSeconds();

// RTC Clock
setVariableValue("@RTC_Year", year);
setVariableValue("@RTC_Month", month);
setVariableValue("@RTC_Day", day);
setVariableValue("@RTC_Hours", hour);
setVariableValue("@RTC_Minutes", min);
setVariableValue("@RTC_Seconds", sec);

/*
print("Date: " + date.toLocaleString());
print("Date: " + date.toString());
print("Date: " + date.toLocaleTimeString());
print("Date: " + date.toLocaleDateString());
print();
*/