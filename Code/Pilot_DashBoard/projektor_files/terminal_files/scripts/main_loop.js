energy();
motor_power(); 
FC_power(); //- Falta FC_voltage
//request_BMS_data();
includeFile("update_datestring.js");
//includeFile("make_warnings.js");



function  motor_power() {
    var motor_voltage = getVariableValue("Motor_voltage") * 0.1;
    var motor_current = getVariableValue("Motor_input_current") * 0.01;
    
    motor_power = motor_voltage * motor_current;
    
    setVariableValue("Motor_power", motor_power);
}

function FC_power() {
    var FC_voltage = getVariableValue("FC_Voltage") * 0.1;
    var FC_current = getVariableValue("IP_value") * 0.001;

    FC_power = FC_voltage * FC_current;

    setVariableValue("FC_power", FC_power);
    
    return FC_power;
}

function  energy() {
    //Time routine
    var time = getVariableValue("Energy_duration");
    time = time + 1;
    setVariableValue("Energy_duration", time); // Should be applied a 0.1 factor
    
    time_string = sec2time(Math.round(time*0.1));
    setVariableValue("Energy_duration_string", time_string);
    
    //Energy routine
    var energy_joules = getVariableValue("Energy_joules");
    energy_joules = energy_joules + FC_power()*0.1; // 0.1 is the interval between calls to script
    var energy_wh = energy_joules / 3600;
    
    setVariableValue("Energy_joules", energy_joules);
    setVariableValue("Energy_Wh", energy_wh);
}

function sec2time(timeInSeconds) {
    var pad = function(num, size) { return ('000' + num).slice(size * -1); },
    time = parseFloat(timeInSeconds).toFixed(3),
    hours = Math.floor(time / 60 / 60),
    minutes = Math.floor(time / 60) % 60,
    seconds = Math.floor(time - minutes * 60),
    milliseconds = time.slice(-3);

    return pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2);
}

function request_BMS_data(){
    // Send messages
    var CANPort = 1;
    var CANID = 0x201;
    var DLC = 8;

    for (let signal = 0X14; signal <= 0x1B; signal++){
        sendCANMessage(CANPort,CANID, DLC, signal, 0, 0, 0, 0, 0, 0, 0);
    }
}
