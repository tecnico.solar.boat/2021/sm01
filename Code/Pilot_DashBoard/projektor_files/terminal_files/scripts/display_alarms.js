// Read Warnings
warn_overvolt = setVariableValue("warn_overvolt");
warn_undervolt = setVariableValue("warn_undervolt");
warn_overtemp70 = setVariableValue("warn_overtemp70");
warn_overtemp80 = setVariableValue("warn_overtemp80");

nr = 0;

// Display Warnings
nr = check_warning(warn_overvolt, "FC Overvoltage (>0.8V)", nr);
nr = check_warning(warn_undervolt, "FC UnderVoltage (<0.3V) Shut off load", nr);
nr = check_warning(warn_overtemp70, "FC Overtemp (<70ºC) Reduce load", nr);
nr = check_warning(warn_overtemp80, "FC Overtemp (<80ºC) Shut off load", nr);

// Set number of Warnings
setVariableValue("nr_warnings", nr);

function check_warning(warning, msg, nr){
    if (warning === 1){
        setVariableValue("@AlarmShow",0x8002);
        setVariableValue("warning_string", msg);
        return nr + 1;
    }
    return nr;
}
