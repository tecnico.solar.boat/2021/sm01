/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/


#ifndef CAN_TSB_H2_h
#define CAN_TSB_H2_h

#if defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
#include "../../avr_can/src/avr_can.h"
#include <Arduino.h>
#elif defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
#include "../utility/FlexCAN_T4.h"
#include <TimeLib.h>
#endif

#include "../utility/ArduinoJson.h"
#include "buffer.h"



#define MAXEQ(A,B) (A) = max((A), (B))

// --------------------------------------------------------

/*Converts enum to Fc errors*/

//We cannot disclose this part, its covered under NDA

// --------------------------------------------------------

/*Converts enum to CurrentSensor errors*/
typedef enum{
    no_error_cs = 0x00,
    Memory_Error = 0x40,
    Overcurrent_Detection = 0x41,
    Fluxgate_has_no_oscillaction_for_more_than_20ms = 0x42,
    Clock_derivation = 0x44,
    Supply_voltage_is_out_of_range = 0x46,
    Hardware_default_ADC_channel = 0x47,
    New_Data_not_available = 0x49,
    Hardware_default_DAC_Treshold = 0x4A,
    Hardware_deafult_Reference_voltage = 0x4B

}CS_error_code;

const char* CS_interface_to_string(CS_error_code fault);

// --------------------------------------------------------

class Throttle_Data{
    public:
        int8_t value;   // Requested Power [-100;100]
        bool zero;      // If set, throttle has gone to zero after startup

        StaticJsonDocument<256> toJson();
};

// --------------------------------------------------------

class Battery_Data{
    public:
        float voltage;                  // Battery Pack Voltage in V
        float temperature;              // Battery Pack Voltafe in ºC
        float current_flow;             // Battery Pack Current in A
        uint8_t state_of_charge;        // Battery SOC in %
        uint8_t bat_state_status;       // Battery Pack State Status
        uint8_t bat_state_temp;         // Battery Pack State Temperature
        uint8_t bat_state_voltage;      // Battery Pack State Voltage

        StaticJsonDocument<256> toJson();
};

// --------------------------------------------------------

class FC_Data{

    public:
        
        //We cannot disclose this part, its covered under NDA

        StaticJsonDocument<256> toJson();
};

// --------------------------------------------------------

class Motor_Data{
    public:
        float motor_current;        // Motor current (quadrature current)
        float input_current;        // VESC Input Current
        float duty_cycle;           // VESC Duty Cycle
        int32_t rpm;                // Motor RPMs 
        float voltage;              // VESC Input Voltage
        float temp_motor;           // Motor Temperature
        float temp_mos_max;         // Highest MOS Temperature DOSE NOT COME FROM THE CAN BUS
        float temp_mos_1;           // VESC MOS 1 Temperature
        float temp_mos_2;           // VESC MOS 2 Temperature
        float temp_mos_3;           // VESC MOS 3 Temperature
        uint8_t fault;              // VESC Fault Code
        uint8_t id;                 // VESC ID
        int16_t asked_current;      // Motors Current Resquested
        
        StaticJsonDocument<512> toJson();	
};

// --------------------------------------------------------

class Screen_Data{
    public:
        // SD card related stuff
        volatile bool newFile;
        
};

// --------------------------------------------------------

class Current_Sensor{
    public:
        int32_t ip_value;                 // FC output current current in mA
        bool error_indication;            // 0 = Normal, 1 = Failure
        CS_error_code error_info;         // Error Code
        uint16_t sensor_name;             // Sensor Name, always returns "CAB500"
        uint8_t sw_revision;              // Software revision

        StaticJsonDocument<512> toJson();	
};

// --------------------------------------------------------

class Fuse_Board_Data{
    public:
        float current_24;       // Current in the 24 V Bus
        float current_48;       // Current in the 48 V Bus
        
        StaticJsonDocument<256> toJson();

};

// --------------------------------------------------------

class Power_Box_Data{
    public:
        float lat;              // Boat Latitude in Decimal Degrees Format
        float lon;              // Boat Longitude in Decimal Degress Format
        float speed;            // Boat Speed in Knots
        uint16_t angle;         // Boat Direction with Noth
        bool FC_Sense;          // 1 menad there is voltage
        bool FC_Relay;          // 1 means Relay is closed 
        bool Motor_Relay;       // 1 means Relay is closed
        bool PreCharge_Relay;   // 1 means Relay is closed
        bool Killswitch;        // 1 means Relay kilswitch is on

        StaticJsonDocument<256> toJson();
};

// --------------------------------------------------------

class H2_Board_Data{
    public:
        uint8_t temp_H2_in;         // Hydrogen FC Inlet Temperature
        uint8_t temp_H2_out;        // Hydrogen FC Outlet Temperature
        uint8_t humid_H2_in;        // Hydrogen FC Inlet Humidity
        uint8_t humid_H2_out;       // Hydrogen FC Outlet Humidity
        uint8_t temp_air_in;        // Air FC Inlet Temperature
        uint8_t temp_air_out;       // Air FC Outlet Temperature
        uint8_t temp_cooling_in;    // Coolant FC Inlet Temperature
        uint8_t temp_cooling_out;   // Coolant FC Outlet Temperature
        float pressure_H2_in;       // Hydrogen FC Inlet Pressure
        float pressure_H2O_out;     // Air FC Outlet Pressure
        uint16_t blower_rpms;       // Air Inlet Blower RPMs 
        float airflow;              // Air Inlet flow
        uint8_t blower_temp;        // Air Inlet Blower motor temperature
        bool H2O_solen;             // H2O Solenoid, 1 menas open (gas can pass by)
        bool H2_solen;              // Hydrogen Solenoid, 1 menas open (gas can pass by)
        bool cooling_pump;          // Cooling Pump, 1 menad pump is ON

    StaticJsonDocument<256> toJson();
};

// --------------------------------------------------------

class TSB_CAN_H2 : public CANListener 
{
private:
    #if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
    static time_t getTeensy3Time();
    #endif
public:
    int *filters;
    int32_t ind = 0;
    uint8_t status;


    Throttle_Data throttle;
    Battery_Data bat;
    FC_Data fc;
    Motor_Data motor;
    Current_Sensor cs;
    Fuse_Board_Data fuse_board;
    Power_Box_Data power_box;
    H2_Board_Data h2_board; 

    TSB_CAN_H2();  
    void receive_Message(CAN_message_t &frame);
    void printFrame(CAN_message_t &frame, int mailbox);
    #if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
        bool frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller); //overrides the parent version so we can actually do something 
    #elif defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
        void gotFrame(CAN_message_t *frame, int mailbox); //overrides the parent version so we can actually do something
    #endif   
};

#endif
