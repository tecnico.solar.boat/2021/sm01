/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>
#include "Relays.h"

/**
 * Relays::Relays 
 */
Relays::Relays(int pinSol1,int pinSol2,int pinWaterPump,int pinSpare)
{
    _pin_sol1 = pinSol1;
    _pin_sol2 = pinSol2;
    _pin_waterPump = pinWaterPump;
    _pin_spare = pinSpare;
    
    /* Set pinMode */
    pinMode(_pin_sol1, OUTPUT);
    pinMode(_pin_sol2, OUTPUT);
    pinMode(_pin_waterPump, OUTPUT);
    pinMode(_pin_spare, OUTPUT);

    /*  Set low */
    digitalWrite(_pin_sol1, _state_sol1);
    digitalWrite(_pin_sol2, _state_sol2);
    digitalWrite(_pin_waterPump, _state_waterPump);
    digitalWrite(_pin_spare, _state_spare);

}

/**
 * Set the state of solenoid 1 and 
 * change the logical value of the pin
 *
 * @param newState LOW to close, HIGH to open it
*/
void Relays::setState_sol1(bool newState)
{
    _state_sol1 = newState;

    digitalWrite(_pin_sol1, newState);
}

/**
 * Set the state of solenoid 2 and 
 * change the logical value of the pin
 *
 * @param newState LOW to close, HIGH to open it
*/
void Relays::setState_sol2(bool newState)
{
    _state_sol2 = newState;

    digitalWrite(_pin_sol2, newState);
}

/**
 * Set the state of water pump and 
 * change the logical value of the pin
 *
 * @param newState LOW to close, HIGH to open it
*/
void Relays::setState_waterPump(bool newState)
{
    _state_waterPump = newState;

    digitalWrite(_pin_waterPump, newState);
}

/**
 * Set the state of spare relay and 
 * change the logical value of the pin
 *
 * @param newState LOW to close, HIGH to open it
*/
void Relays::setState_spare(bool newState)
{
    _state_spare = newState;

    digitalWrite(_pin_spare, newState);
}