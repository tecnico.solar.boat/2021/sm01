/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/


#include "CAN_TSB_H2.h"

TSB_CAN_H2::TSB_CAN_H2()
{
	this->fc.cell_volt = new int16_t[40]();
}

void TSB_CAN_H2::receive_Message(CAN_message_t &frame)
{
	switch(frame.id) {
		//**********************
		//***** Throttle  ******
		//**********************
		case 0x10:
			ind = 0;
			this->throttle.value = buffer_get_int8(frame.buf, &ind);
			this->throttle.zero = (bool) (buffer_get_uint8(frame.buf, &ind) & 0x1);
		break;
		
		//*****************
		//***** BMS  ******
		//*****************
        
		// Not implemented on this vessel

		//*****************
		//****** FC *******
		//*****************

		// We cannot disclose this part, its covered under NDA

		//*********************
		//******* Motor *******
		//*********************
		case 0x901:
			ind = 0;		
			this->motor.motor_current = buffer_get_float32(frame.buf, 1e2, &ind);
			this->motor.input_current = buffer_get_float32(frame.buf, 1e2, &ind);
			break;

		case 0xE01:
			ind = 0;
			this->motor.duty_cycle = buffer_get_float16(frame.buf, 1e3, &ind);
			this->motor.rpm = buffer_get_int32(frame.buf, &ind)/5;
			this->motor.voltage = buffer_get_float16(frame.buf, 1e1, &ind);
			break;

		case 0xF01:
			ind = 0;
			this->motor.temp_mos_1 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor.temp_mos_2 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor.temp_mos_3 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor.temp_mos_max = buffer_get_float16(frame.buf, 1e1, &ind);
			break;
		case 0x1001:
			ind = 0;
			this->motor.fault = buffer_get_uint8(frame.buf, &ind);
			this->motor.id = buffer_get_uint8(frame.buf, &ind);
			break;

		//*****************
		//****** CS *******
		//*****************
        
		case 0x606:
			ind = 0;  
			if (frame.buf[0] < 0x80)
			    this->cs.ip_value = -1*(uint8_t)(0x80000000 - buffer_get_int32(frame.buf, &ind));   /*mA (negative)*/
			else
			    this->cs.ip_value = buffer_get_int32(frame.buf, &ind) - 0x80000000;				/*mA (positive)*/
			this->cs.error_indication = (bool) (frame.buf[4] & 0x1);						    /*1 = error*/
			if (cs.error_indication != 0)
				this->cs.error_info = static_cast<CS_error_code>(frame.buf[4] >> 1);
			else
				this->cs.error_info = static_cast<CS_error_code>(0x00);
			this->cs.sensor_name = (frame.buf[5] << 8 ) | frame.buf[6];
			this->cs.sw_revision = frame.buf[7];
		break;

		//*************************
		//****** Fuse Board *******
		//*************************

		case 0x607:
			ind = 0;
			this->fuse_board.current_24 = buffer_get_float8(frame.buf, 1e1, &ind);
			this->fuse_board.current_48 = buffer_get_float8(frame.buf, 1e1, &ind);
		break;

		//*************************
		//****** Power Box ********
		//*************************
		
		case 0x618:
			ind = 0;
			this->power_box.lat = buffer_get_float32(frame.buf, pow(2,24), &ind);
			this->power_box.lon = buffer_get_float32(frame.buf, pow(2,23), &ind);
		break;

		case 0x628:
			ind = 0;
			this->power_box.speed = buffer_get_float16(frame.buf, 1e2, &ind);
			this->power_box.angle = buffer_get_uint16(frame.buf, &ind);
			status = buffer_get_uint8(frame.buf, &ind);
			this->power_box.Killswitch = (bool) ((status & 0x16) >> 4);
			this->power_box.FC_Sense = (bool) ((status & 0x8) >> 3);
			this->power_box.FC_Relay = (bool) ((status & 0x4) >> 2);
			this->power_box.Motor_Relay = (bool) ((status & 0x2) >> 1);
			this->power_box.PreCharge_Relay = (bool) ((status & 0x1) >> 0);
		break;

		//***********************
		//****** H2 Board *******
		//***********************

		case 0x619:
			ind = 0;
			this->h2_board.temp_H2_in = buffer_get_uint8(frame.buf, &ind);
			this->h2_board.temp_H2_out = buffer_get_uint8(frame.buf, &ind);
			this->h2_board.humid_H2_in = buffer_get_uint8(frame.buf, &ind);
			this->h2_board.humid_H2_out = buffer_get_uint8(frame.buf, &ind);
			this->h2_board.temp_air_in = buffer_get_uint8(frame.buf, &ind);
			this->h2_board.temp_air_out = buffer_get_uint8(frame.buf, &ind);
			this->h2_board.temp_cooling_in = buffer_get_uint8(frame.buf, &ind);
			this->h2_board.temp_cooling_out = buffer_get_uint8(frame.buf, &ind);
		break;

		case 0x629:
			ind = 0;
			this->h2_board.pressure_H2_in = buffer_get_float8(frame.buf, 1e1, &ind);
			this->h2_board.pressure_H2O_out = buffer_get_float8(frame.buf, 1e1, &ind);
			this->h2_board.blower_rpms = buffer_get_uint16(frame.buf, &ind);
			this->h2_board.airflow = buffer_get_uint16(frame.buf, &ind);
			this->h2_board.blower_temp = buffer_get_uint8(frame.buf, &ind);
			status = buffer_get_uint8(frame.buf, &ind);
			this->h2_board.H2O_solen = (bool) (status & 0x4) >> 2;
			this->h2_board.H2_solen = (bool) (status & 0x2) >> 1;
			this->h2_board.cooling_pump = (bool) (status & 0x1) >> 0;
		break;

    }
}

void TSB_CAN_H2::printFrame(CAN_message_t &msg, int mailbox)
{
	// For AVR:
	#if defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
	Serial.print("MB: ");
	if (mailbox > -1) Serial.print(mailbox);
	else Serial.print("???");
	Serial.print(" ID: 0x");
	Serial.print(msg.id, HEX);
	Serial.print(" Len: ");
	Serial.print(msg.len);
	Serial.print(" Data: 0x");
	for (int count = 0; count < msg.len; count++) {
		Serial.print(msg.buf[count], HEX);
		Serial.print(" ");
	}
	Serial.print("\r\n");  

	// For Teensy:
	#elif defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
	Serial.print("MB "); Serial.print(msg.mb);
	Serial.print("  OVERRUN: "); Serial.print(msg.flags.overrun);
	Serial.print("  LEN: "); Serial.print(msg.len);
	Serial.print(" EXT: "); Serial.print(msg.flags.extended);
	Serial.print(" TS: "); Serial.print(msg.timestamp);
	Serial.print(" ID: "); Serial.print(msg.id, HEX);
	Serial.print(" Buffer: ");
	for ( uint8_t i = 0; i < msg.len; i++ ) {
		Serial.print(msg.buf[i], HEX); Serial.print(" ");
	} Serial.println();
	#endif
}

#if defined(__IMXRT1062__) || defined(__MK20DX256__) || defined(__MK64FX512__) || defined(__MK66FX1M0__)
bool TSB_CAN_H2::frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller)
{
		// printFrame(frame, mailbox);
		receive_Message(frame);
		return true;
}

#elif defined(__AVR_AT90CAN32__) || defined(__AVR_AT90CAN64__) || defined(__AVR_AT90CAN128__)
void TSB_CAN_H2::gotFrame(CAN_message_t *frame, int mailbox)
{
	// printFrame(*frame, mailbox);
	receive_Message(*frame);
	return;
}
#endif



/*Converts FC errors to string*/

//We cannot disclose this part, its covered under NDA

/*Converts CS errors to string*/
const char* CS_interface_to_string(CS_error_code fault)
{
	switch(fault) {
		case no_error_cs: return "No error"; break;
		case Memory_Error: return "Memory Error"; break;
		case Overcurrent_Detection: return "Overcurrent Detection"; break;
		case Fluxgate_has_no_oscillaction_for_more_than_20ms: return "Fluxgate has no oscillaction for more than 20ms"; break;
		case Clock_derivation: return "Clock derivation"; break;
		case Supply_voltage_is_out_of_range: return "Supply voltage is out of range"; break;
		case Hardware_default_ADC_channel: return "Hardware default ADC channel"; break;
		case New_Data_not_available: return "New Data not available"; break;
		case Hardware_default_DAC_Treshold: return "Hardware_default_DAC_Treshold"; break;
		case Hardware_deafult_Reference_voltage: return "Hardware deafult Reference voltage"; break;
	}
	return "This error code is not specified!";
}
