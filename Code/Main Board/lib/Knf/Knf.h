/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef KNF_h
#define KNF_h

#include <Arduino.h>

/**
 * Struct that holds all the information about the knf pump
*/
struct Knf
{
private:
     bool _state = LOW;
     int16_t _targetSpeed;

     //TODO: ler dados da knf
     int16_t _actualSpeed;
     int _current;
     int _temperature;

public:
     Knf();
     byte start();
     byte stop();
     byte setSpeed(int);
     byte getInfo();

     /**
         * Get current state of knf pump 
         * @return {bool}  : current state of knf pump
         */
     bool getState() { return _state; }

     /**
         * Get target speed of knf pump
         * @return {uint8_t}  : current target speed 
         */
     uint16_t getTargetSpeed() { return _targetSpeed; }

     /**
         * Get actual speed of knf pump
         * @return {uint8_t}  : current actual speed
         */
     uint16_t getActualSpeed() { return _actualSpeed; }

     /**
         * Get current pulled by knf pump  
         * @return {int}  : current
         */
     int getCurrent() { return _current; }

     /**
         * Get current temperature inside knf pump 
         * @return {int}  : current
         */
     int getTemperature() { return _temperature; }
};
#endif