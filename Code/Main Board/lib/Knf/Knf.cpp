/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>
#include "Knf.h"

/**
 * Class constructer 
 */
Knf::Knf()
{
    /* Serial start */
    Serial1.begin(57600);
}

/**
 * Start knf pump. 
 * 
 * @return {byte}  : 0->Command not completed, 1->Command completed 
 */
byte Knf::start()
{
    byte lastByte = 'j';
    byte lastByte2 = 'j';
    _state = HIGH;

    // Write to serial "dB <CR>"
    Serial1.write('d');
    Serial1.write('B');
    Serial1.write('\r');

    while (Serial1.available())
    {
        lastByte2 = lastByte;
        lastByte = Serial1.read();
    }
    
    return lastByte2;
}

/**
 * Stop knf pump
 * 
 * @return {byte}  : 0->Command not completed, 1->Command completed 
 */
byte Knf::stop()
{

    byte lastByte = 'j';
    _state = LOW;

    // Write to serial "dE <CR>"
    Serial1.write('d');
    Serial1.write('E');
    Serial1.write('\r');

    return lastByte;
}

/**
 * Read info from knf pump
 * 
 * @return {byte}  : 0->Command not completed, 1->Command completed
 */
byte Knf::getInfo()
{
    // Write to serial "dE <CR>"
    Serial1.write('d');
    Serial1.write('E');
    Serial1.write('\r');

    if(Serial1.available())
    {
        Serial.println(Serial1.read());
    }
}

/**
 * Set new speed 
 * @param  newValue new speed as nnnn
 * @return {byte}         : 0->Command not completed, 1->Command completed
 */
byte Knf::setSpeed(int newValue)
{
    byte lastByte = 'j';
    _targetSpeed = newValue;

    // Write to serial "dE <CR>"
    Serial1.write('d');
    Serial1.write('S');
    Serial1.print(newValue);
    Serial1.write('\r');

    long lastTime = millis();

    // Wait for response
    while (!Serial1.available())
    {
        if ((millis() - lastTime) >= 1000)
        {
            break;
        }
    }
    // Read from serial
    while (Serial1.available())
    {
        // Only save the last byte
        lastByte = Serial1.read();
    }
    // return if the command was completed or not
    return lastByte;
}
