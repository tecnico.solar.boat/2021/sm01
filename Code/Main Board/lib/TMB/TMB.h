/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef TMB_h
#define TMB_h

#include <Arduino.h>

#include "Knf.h"
#include "Micronel.h"
#include "Hts.h"
#include "Relays.h"

class TMB
{
private:
    // States
    bool _powerMode = 0;

public:
    /**
     * Constructer
    */
    TMB(double *, double *);

    /**
     * Pins used by Teensy Main Board
    */
    enum Pin
    {
        RX = 0,
        TX = 1,
        RST = 2,
        CAN_TX = 3,
        CAN_RX = 4,
        CTRL_SOL1 = 5,
        CTRL_SOL2 = 6,
        CTRL_WATERPUMP = 7,
        CTRL_SPARERELAY = 8,
        INT = 9,
        CS = 10,
        SDI = 11,
        SDO = 12,
        TACHOMETER_AR = 13,
        SCK = 14,
        SDA = 18,
        SCL = 19,
    };

    /**
     * Subclasses
     */
    Relays *relays;
    Micronel *micronel;
    Knf *knf;
    Hts *hts;

    int readStates();
    bool isKillswitchOn();
    bool isPowerMode() { return _powerMode; }

    void setPowerMode(bool newState) { _powerMode = newState; }
};

#endif