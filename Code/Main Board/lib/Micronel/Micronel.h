/*
    Copyright (C) 2021  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef MICRONEL_h
#define MICRONEL_h

#include <Arduino.h>
#include "Adafruit_MCP4725.h"
#include "../PID_v1/src/PID_v1.h"

/**
 * Struct that holds all the information about the micronel blower
*/
struct Micronel
{
private:
    // Temperature limits
    double _MAXTEMP = 60.0;
    double _MAXSTARTTEMP = 50.0;

    // PID gains
    double _Kp = 0.00412055013206722;
    double _Ki = 0.0230573278926859;
    double _Kd = 0;

    Adafruit_MCP4725 dac;

    bool _state = 0;
    double *_currentTemp;
    double *_currentAirFlow;

public:
        Micronel(double *, double *);

    void start();
    void stop();

    uint16_t getVoltage16(double);
    void setVoltage(uint16_t);

    /**
     * Get current state
     * @return {bool}  : current state
     */
    bool getState() { return _state; }

    /**
     * Returns 1 if temperature is under MAXTEMP and 0 if is over
     * @return {bool}           : 1 if temperature is under MAXTEMP and 0 if is over
     */
    bool isTempUnderThreshold() { return (*_currentTemp < _MAXTEMP); }

    /**
     * Get current temperature inside blower
     * @return {double}  : current temperature inside blower
     */
    double getTemp() { return *_currentTemp; }

    /**
     * Get current air flow inside system
     * @return {double}  : current air flow inside system
     */
    double getFlow() { return *_currentAirFlow; }
};

#endif